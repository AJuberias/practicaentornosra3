package main;

import java.util.Scanner;

public class Password {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner(System.in); // Llamamos al scanner lector
		String password="";
		
		
		menu();
		int longitud=IntroduceLongitud(lector); // Conseguimos mediante un m�todo la longitud de la password
		int tipoPassword=tipoPassword(lector); // Conseguimos mediante un m�tido el tipo de password
		
		switch (tipoPassword) {
		case 1:
			password=opcion1(longitud);
			break;
		case 2:
			password=opcion2(longitud);
			break;
		case 3:
			password=opcion3(longitud);
			break;
		case 4:
			password=opcion4(longitud);
			break;
		}
		
		// Para cada caso hemos creado un m�todo

		System.out.println(password);
		lector.close();
	}
	
	public static char caracterRaro(){
		return (char) ((Math.random() * 15) + 33);
	}
	
	public static char letraRandom(){
		return (char) ((Math.random() * 26) + 65);
	}
	
	public static int intRandom(){
		return (int) (Math.random() * 10);
	}
	
	public static String opcion1(int longitud){
		
		String password="";	
		
		for (int i = 0; i < longitud; i++) {
			password += letraRandom();
		}
		
		return password;
	}
	
	public static String opcion2(int longitud){
		
		String password="";
		for (int i = 0; i < longitud; i++) {
			
			password += intRandom();
		}
		
		return password;
	}
	
	public static String opcion3(int longitud){
		
		String password="";
		int n;
		
		for (int i = 0; i < longitud; i++) {
			
			n = (int) (Math.random() * 2);
			if (n == 1) {	
				password += letraRandom();
			}
			else {
				
				password += caracterRaro();
			}
		}
		return password;
	}

	public static String opcion4(int longitud){
	
	String password="";
	
	for (int i = 0; i < longitud; i++) {
		int n;
		n = (int) (Math.random() * 3);
		if (n == 1) {
			password += letraRandom();
		} else if (n == 2) {
			password += letraRandom();
		} else {
			password += intRandom();
		}
	}
	return password;
}
	
	public static int tipoPassword(Scanner lector){
		
		System.out.println("Elige tipo de password: ");
		int opcion = lector.nextInt();
		
		return opcion;
	}
	
	public static int IntroduceLongitud(Scanner lector){
		System.out.println("Introduce la longitud de la cadena: ");
		
		int longitud = lector.nextInt();
		return longitud;
	}
	
	public static void menu(){ // Creamos un m�todo para el inicio
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
	}

}
