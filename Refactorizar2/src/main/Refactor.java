package main;

/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

import java.io.*;
import java.util.*;

public class Refactor {

	static Scanner lector; //a pasa a llamarse Lector
	public static void main(String[] args) 
	{
	
		lector = new Scanner(System.in);
		
		int num; //n pasa a ser "num" y eliminamos cantidad m�ximo de alumnos ya que no se usa
		
		int arrays[] = new int[10];
		for(num=0; num<10; num++) // Separamos con espacios las condiciones del for
		{
			System.out.println("Introduce nota media de alumno");
			arrays[num] = lector.nextInt();
		}	
		
		System.out.println("El resultado es: " + recorrer_array(arrays));
		
		lector.close();
	}
	static double recorrer_array(int vector[])
	{
		double num3 = 0; //c pasa ser num3
		for(int num1=0; num1<10; num1++) //a pasa a ser num1 y separamos las condiciones del for
		{
			num3=num3+vector[num1];
		}
		return num3/10;
	}
	
}