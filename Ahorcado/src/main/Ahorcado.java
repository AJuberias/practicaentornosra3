package main;

import java.io.File;
import java.util.Scanner;

public class Ahorcado {
	
	private final static byte NUM_PALABRAS = 20;
	private final static byte FALLOS = 7;
	private static String[] palabras = new String[NUM_PALABRAS];
	
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		String ruta = "src\\palabras.txt";

		String palabraObtenida=sacarPalabra(ruta);

		char[][] caracteresPalabraBuscada = new char[2][];
		caracteresPalabraBuscada[0] = palabraObtenida.toCharArray();
		caracteresPalabraBuscada[1] = new char[caracteresPalabraBuscada[0].length];

		String totalCaracteres = "";
		int fallos=0;
		boolean acertado;
		boolean encontrado=false;	
		System.out.println("Prueba a acertar la palabra:");
				
		do {

			mostrarPalabraOculta(caracteresPalabraBuscada);

			System.out.println("Di una letra o prueba a acertar la palabra");
			System.out.println("Total de caracteres: " + totalCaracteres);
			
			totalCaracteres= totalCaracteres+input.nextLine().toUpperCase();
			fallos=0;
			fallos=verificar(fallos, totalCaracteres, encontrado, caracteresPalabraBuscada);
			
			ahorcado(fallos);
			System.out.println(fallos);
			acertado=true;
		} 
		while (!finJuego(fallos, palabraObtenida, acertado, caracteresPalabraBuscada));
		input.close();
	}
	
public static boolean finJuego(int fallosJugador, String palabraGanadora, boolean estado, char[][] caracteresPalabraObtenida){
		
		if (fallosJugador >= FALLOS){
			System.out.println("Has perdido: " + palabraGanadora);
			return true;
		}
		
		estado = true;

		for (int i = 0; i < caracteresPalabraObtenida[1].length; i++){
			if (caracteresPalabraObtenida[1][i] != '1'){
				estado = false;
				break;
			}
		}
		
		if (estado==true){
			System.out.println("�Bingo! ");
			return true;
		}
		return false;
	}
	
	
	
	public static void mostrarPalabraOculta(char[][] caracteresPalabraObtenida){

		for (int i = 0; i < caracteresPalabraObtenida[0].length; i++){
			if (caracteresPalabraObtenida[1][i] != '1'){
				System.out.print(" -");
			} 
			
			else{
				System.out.print(" " + caracteresPalabraObtenida[0][i]);
			}
		}
		System.out.println();
	}
	
	public static int verificar(int totalFallos, String caracteresSeleccionados, boolean estado, char[][] caracteresPalabraObtenida){	
		
		for (int j = 0; j < caracteresSeleccionados.length(); j++){
			estado = false;
			
			for (int i = 0; i < caracteresPalabraObtenida[0].length; i++){
				if (caracteresPalabraObtenida[0][i] == caracteresSeleccionados.charAt(j)){
					caracteresPalabraObtenida[1][i] = '1';
					estado = true;
				}
			}
			
			if (!estado)
			{
				totalFallos++;
			}
		}
		
		return totalFallos;
	}
	
	public static String sacarPalabra(String ruta){
		String palabraClave;
		File fichero = new File(ruta);
		Scanner inputFichero = null;

		try{
			inputFichero = new Scanner(fichero);
			
			for (int i = 0; i < NUM_PALABRAS; i++){
				palabras[i] = inputFichero.nextLine();	
			}
		} 
		
		catch (Exception e){
			System.out.println(e.getMessage());
		} 
		
		finally{
			if ((fichero!=null) && (inputFichero!=null)){
				inputFichero.close();
			}
		}
		
		palabraClave = palabras[(int) (Math.random() * NUM_PALABRAS)];
		return palabraClave;
	}
	
	public static void ahorcado(int fallosJugador){	
		switch (fallosJugador) 
		{
		case 1:
			System.out.println("     ___");
		break;
		
		case 2:
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
		break;
		
		case 3:
			System.out.println("  ____ ");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
		break;
		
		case 4:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
		break;
		
		case 5:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println("      |");
			System.out.println("     ___");
		break;
		
		case 6:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println("     ___");
		break;
		
		case 7:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println(" A   ___");
		break;
		}
	}
}