/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

import java.*;
import java.util.*;
/**
 * 
 * @author Andr�s Juber�as Bag��s
 *
 */

public class Refac {
	public static void main(String[] args) {
		int num1;
		int num2;
		int num3; // a y b pasan a ser num1 y num2 para que se entienda mejor qu� representan
		String cadena1;
		String cadena2;
		Scanner lector = new Scanner(System.in); // c pasa a ser Lector, para que se entienda mejor su funci�n
		
		System.out.println("Bienvenido al programa"); //cad s�lo se usaba una vez y por lo tanto ocupada dos l�neas en vez de una
		System.out.println("Introduce tu dni");
		cadena1 = lector.nextLine();
		System.out.println("Introduce tu nombre");
		cadena2 = lector.nextLine();
		
		num1=7;
		num2=16;
		num3 = 25;
		if(num1>num2 || num3%5!=0 && (num3*3-1)>num2/num3) //A�adimos separaciones para que se pueda leer m�s claro las condiciones del if
		{
			System.out.println("Se cumple la condici�n");
		}
		
		num3 = num1+num2*num3+num2/num1;
		
		String array[] = new String[7];
		array[0] = "Lunes";
		array[1] = "Martes";
		array[2] = "Miercoles";
		array[3] = "Jueves";
		array[4] = "Viernes";
		array[5] = "Sabado";
		array[6] = "Domingo";
		
		recorrer_array(array);
	}
	static void recorrer_array(String vectorDeStrings[]) //Ponemos una mayuscula cada vez que cambiamos de palabra para que se lea mejor
	{
		for(int dia=0; dia<7; dia++) //Ponemos separaciones para que se lea mejor las condiciones del bucle for
		{
			System.out.println("El dia de la semana en el que te encuentras ["+(dia+1)+"-7] es el dia: "+vectorDeStrings[dia]);
		}
	}
	
}